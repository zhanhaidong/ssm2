package com.controller;

import com.dao.StudentMapper;
import com.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zhd on 2018/4/9.
 */
@Controller
public class Test {

    @Autowired public StudentMapper studentMapper;

    @ResponseBody
    @RequestMapping("/test")
    public Student itmeList2(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Student student= studentMapper.select();
        System.out.println( studentMapper.select());
        return student;
    }


}
